
$(document).mousemove(function(e){
  $('#mousemove').text(e.pageX + ', ' + e.pageY);
});

$(document).mousedown(function(e){
       if(event.which == 1) message = ' left';
  else if(event.which == 2) message = ' middle';
  else if(event.which == 3) message = ' right';
  else                      message = ' weird'
  $('#mousedown').text(e.pageX + ', ' + e.pageY+message+', '+time+'ms');
});

$(document).mouseup(function(e){
  $('#mouseup').text(e.pageX + ', ' + e.pageY+', '+time+'ms');
});

$(document).keydown(function(e){
  var key = (e.keyCode ? e.keyCode : e.which);
  $('#key').text('\''+key+'\', '+time+'ms');
});

$(window).scroll(function(e){
  $('#scroll').text(getScrollXY()+', '+time+'ms');
});

var time  = 0;
$(document).ready(function(e){
  $('#debug').append($('<pre></pre>').css('position','fixed').css('right','10px')
    .append($('<span>resolution: </span><span id="resolution"></span><br>'))
    .append($('<span>size:       </span><span id="size"></span><br>'))
    .append($('<span>mousemove:  </span><span id="mousemove"></span><br>'))
    .append($('<span>mousedown:  </span><span id="mousedown"></span><br>'))
    .append($('<span>mouseup:    </span><span id="mouseup"></span><br>'))
    .append($('<span>key:        </span><span id="key"></span><br>'))
    .append($('<span>time:       </span><span id="time"></span><br>'))
    .append($('<span>scroll:     </span><span id="scroll"></span>')));
  $('#resolution').text(screen.width + ', ' + screen.height);
  $('#size').text($(document).width() + ', ' + $(document).height());
  setInterval('tick()', 1);
});

function tick() {
    $('#time').text((time+=1)+'ms');
}

function getScrollXY() {
    var scrOfX = 0, scrOfY = 0;
    if( typeof( window.pageYOffset ) == 'number' ) {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
    } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
    } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
    }
    return [ scrOfX, scrOfY ];
}
