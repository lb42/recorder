import os

from flask import Flask, render_template, request, make_response

from foo import bar

app = Flask(__name__)

@app.route('/foobar')
def foobar():
	return bar()

@app.route('/')
def start():
	return render_template('index.html')

if __name__ == '__main__':
	# Bind to PORT if defined, otherwise default to 5000.
	port = int(os.environ.get('PORT', 5002))
	app.debug = True
	app.run(host='0.0.0.0', port=port)